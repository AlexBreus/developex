# -*- coding: utf-8 -*-
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import FormView, TemplateView, RedirectView, DetailView
from django.contrib.auth import login, logout

from braces.views import LoginRequiredMixin

from .forms import AuthView, LoginForm, WithdrawForm
from .models import Transaction
from .mixins import RenderErrorMixin


class LoginView(RenderErrorMixin, FormView):
    template_name = 'login.html'
    form_class = LoginForm

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('operations')

        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('auth', kwargs={'card_number': self.card_number})

    def form_valid(self, form):
        self.card_number = form.cleaned_data['card_number']

        return super(LoginView, self).form_valid(form)


class AuthView(FormView):
    template_name = 'auth.html'
    form_class = AuthView

    def get_success_url(self):
        return reverse('operations')

    def get_initial(self):
        initial = super(AuthView, self).get_initial()
        initial['card_number'] = self.kwargs.pop('card_number')

        return initial

    def form_valid(self, form):
        login(self.request, form.user)

        return super(AuthView, self).form_valid(form)


class OperationsView(LoginRequiredMixin, TemplateView):
    template_name = 'operations.html'


class LogoutView(RedirectView):
    http_method_names = [u'post']
    permanent = False
    url = '/'

    def post(self, request, *args, **kwargs):
        logout(request)

        return super(LogoutView, self).post(request, *args, **kwargs)


class WithdrawView(LoginRequiredMixin, RenderErrorMixin, FormView):
    template_name = 'withdraw.html'
    form_class = WithdrawForm

    def get_success_url(self):
        return reverse('result', kwargs={'pk': self.transaction.pk})

    def get_form_kwargs(self):
        kwargs = super(WithdrawView, self).get_form_kwargs()
        kwargs['instance'] = self.request.user

        return kwargs

    def form_valid(self, form):
        form.save()
        self.transaction = form.transaction

        return super(WithdrawView, self).form_valid(form)


class BalanceView(LoginRequiredMixin, TemplateView):
    template_name = 'balance.html'

    def get(self, request, *args, **kwargs):
        Transaction.objects.create(
            operation='balance',
            card=request.user,
        )

        return super(BalanceView, self).get(request, *args, **kwargs)


class OperationResultView(LoginRequiredMixin, DetailView):
    template_name = 'result.html'
    model = Transaction
