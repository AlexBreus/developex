from django import forms
from django.contrib.auth import authenticate

from .models import CardUser
from .models import Transaction


class LoginForm(forms.Form):
    card_number = forms.CharField(max_length=16, min_length=16, required=False, widget=forms.HiddenInput(attrs={'id': "card_number_id"}))

    def clean(self):
        data = super(LoginForm, self).clean()
        try:
            user = CardUser.objects.get(card_number=self.cleaned_data['card_number'])
        except (CardUser.DoesNotExist, KeyError):
            raise forms.ValidationError("Карта не существует")

        if user.is_blocked:
            raise forms.ValidationError("Карта заблокированая")

        return data


class AuthView(forms.Form):
    card_number = forms.CharField(max_length=16, min_length=16, widget=forms.HiddenInput())
    password = forms.CharField(max_length=4, min_length=4, widget=forms.HiddenInput(attrs={'id': "card_pin_id"}))

    def clean(self):
        data = super(AuthView, self).clean()
        user = authenticate(**self.cleaned_data)

        if user:
            self.user = user
            user.wrong_tries = 0
            user.save()
        else:
            try:
                user = CardUser.objects.get(card_number=self.cleaned_data['card_number'])
                user.wrong_tries += 1

                if user.wrong_tries >= 4:
                    user.is_blocked = True
                    user.save()

                    raise forms.ValidationError("Вы ввели 4 неправильных пин-кода и Ваша карта заблокирована")

                user.save()

            except (CardUser.DoesNotExist, KeyError):
                pass

            raise forms.ValidationError("Неверный пин-код")

        if user.is_blocked:
            raise forms.ValidationError("Карта заблокирована")

        return data


class WithdrawForm(forms.Form):
    amount = forms.IntegerField(min_value=0, widget=forms.HiddenInput(attrs={'id': "amount_id"}))

    def __init__(self, *args, **kwargs):
        self.instance = kwargs.pop('instance', None)
        super(WithdrawForm, self).__init__(*args, **kwargs)

    def clean(self):
        data = super(WithdrawForm, self).clean()
        amount = self.cleaned_data.get('amount')

        if not self.instance or not amount:
            return data

        if amount > self.instance.balance:
            raise forms.ValidationError("Недостаточно денег на счету")

        return data

    def save(self):
        if self.is_valid():
            self.instance.balance -= self.cleaned_data['amount']
            self.instance.save()
            self.transaction = Transaction.objects.create(
                operation='take_cash',
                card=self.instance,
                amount=self.cleaned_data['amount'],
            )
