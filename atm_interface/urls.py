from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.LoginView.as_view(), name='login'),
    re_path(r'^auth/(?P<card_number>\d{16})/$', views.AuthView.as_view(), name='auth'),
    path('operations/', views.OperationsView.as_view(), name='operations'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('withdraw/', views.WithdrawView.as_view(), name='withdraw'),
    path('balance/', views.BalanceView.as_view(), name='balance'),
    re_path('^result/(?P<pk>[0-9]+)/$', views.OperationResultView.as_view(), name='result'),
]