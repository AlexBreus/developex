from django.db import models
from django.core import validators
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class CardUserManager(BaseUserManager):
    use_in_migrations = True


class CardUser(AbstractBaseUser):
    card_number = models.CharField(
        max_length=16,
        unique=True,
        validators=[
            validators.RegexValidator(
                '\d{16}',
                "Номер должен быть длиной 16 и состоять только из цифр"
                )
            ]
        )
    balance = models.IntegerField()
    wrong_tries = models.IntegerField(default=0)
    is_blocked = models.BooleanField()

    objects = CardUserManager()

    USERNAME_FIELD = 'card_number'

    class Meta:
        db_table = 'cards'

    def get_absolute_url(self):
        return '/'

    def get_username(self):
        return self.card_number


TRANSACTION_TYPES = (
    ('take_cash', 'Снятие денег'),
    ('balance', 'Проверка баланса'),
)


class Transaction(models.Model):
    operation = models.CharField(max_length=10, choices=TRANSACTION_TYPES)
    card = models.ForeignKey(CardUser, on_delete=models.CASCADE)
    amount = models.PositiveIntegerField(blank=True, null=True)
    date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'transactions'
